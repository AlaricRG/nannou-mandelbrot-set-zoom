use nannou::prelude::*;
use nannou::image::{ RgbImage, DynamicImage, Rgb };
use num_complex::Complex;

// TODO : passer les dimensions de la fenetre en constantes

fn main() {
    nannou::app(model).update(update).run();
}

struct Model {
    texture: wgpu::Texture, // Texture drawn on the window
    _window: window::Id, // the window
    scale: f32, // zoom scale
    mouse_x: f32, // mouse position x
    mouse_y: f32, // mouse position y
}

// Draw the mandelbrot set on an image and return it
fn get_texture(w: u32, h: u32, scale: f32, mouse_x: f32, mouse_y: f32) -> DynamicImage {

    let mut img = RgbImage::new(w, h); // image on wich we will draw

    let max_iter: u32 = 200; // Numbers if iterations

    /* For mandelbrot set we only look at the graph
     between x = -2 and x = 2, y = -2 and y = 2 */
    let cxmin: f32 = -2.0;
    let cymin: f32 = -2.0;

    //let cxmax: f32 = 1.0;
    //let cymax: f32 = 1.5;

    // Zoom scale
    let scalex = scale;
    let scaley = scale;

    // For all pixels of the window
    // Mandelbrot set drawing
    for x in 0..w {
        for y in 0..h {
            let cx = cxmin + x as f32 * scalex + mouse_x * scale;
            let cy = cymin + y as f32 * scaley + mouse_y * scale;

            let c = Complex::new(cx, cy);
            let mut z = Complex::new(0.0, 0.0);

            let mut i = 0;
            for t in 0..max_iter {
                if z.norm() > 2.0 {
                    break;
                }
                z = z * z + c;
                i = t
            }

            // Color of the pixel
            /* more iteration => more bright
            If i = 0 ==> black, else grayscale proportionnaly to max_iter */
            let color: u8 = match i {
                0 => 0,
                _ => (i * 255 / max_iter) as u8
            };

            // Set color to pixel
            img.put_pixel(x, y, Rgb([color, color/10, color/10]));
        }
    }

    // Return
    DynamicImage::ImageRgb8(img)
}

// Initialisation of the model
fn model(app: &App) -> Model {

    // Window size
    let win_w: u32 = 1280;
    let win_h: u32 = 720;

    // Create window
    let _window = app
        .new_window()
        .size(win_w, win_h)
        .title("Mandelbrot set")
        .view(view)
        .mouse_wheel(mouse_wheel) // get mouse wheel events
        .mouse_moved(mouse_moved) // get mouse move events
        .build()
        .unwrap();

    // Base zoom scale
    let scale: f32 = 0.004;

    // Default mouse position to 0 0
    let mouse_x: f32 = 0.0;
    let mouse_y: f32 = 0.0;

    // Create texture on wich we draw pixels
    let texture = wgpu::Texture::from_image(app, &get_texture(win_w, win_h, scale, mouse_x, mouse_y));

    // Return model
    Model { texture, _window, scale, mouse_x, mouse_y}
}

// Update texture each frame
fn update(_app: &App, _model: &mut Model, _update: Update) {
    _model.texture = wgpu::Texture::from_image(_app, &get_texture(1280, 720, _model.scale, _model.mouse_x, _model.mouse_y));
}

// Management of mouse wheel events
fn mouse_wheel(_app: &App, _model: &mut Model, _dt: MouseScrollDelta, _phase: TouchPhase) {
    //println!("Scroll event : {:?}", _dt);
    //println!("Scale : {}", _model.scale);

    // Mouse wheel change scale
    if let MouseScrollDelta::LineDelta(_x, y) = _dt {
        _model.scale += y / 10000.0;
    }
}

// Management of mouse movement events
fn mouse_moved(_app: &App, _model: &mut Model, _pos: Point2) {
    //println!("Mouse pos : {}", _pos);

    // Move image with mouse
    _model.mouse_x = _pos.x;
    _model.mouse_y = -_pos.y;
}

// Display things on the window
fn view(app: &App, model: &Model, frame: Frame) {
    frame.clear(BLACK);
    let draw = app.draw();

    // Draw the texture we created
    draw.texture(&model.texture);

    // WRITE TO WINDOW FRAME
    draw.to_frame(app, &frame).unwrap();
}
