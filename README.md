# Nannou Mandelbrot set zoom

Small exercice for Artificial Intelligence course of 3rd year of IMAC engineering school (ESIPE, Gustave Eiffel University, France).
Mandelbrot set vizualisation written in rust with the [nannou creative coding framework](https://nannou.cc/).

Run with `cargo run`.
